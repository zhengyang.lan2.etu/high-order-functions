def is_even(x):
    """
    Calculate if a number is even or not
    :param x: input
    :return: a boolean indicating if the number if even
    """
    return x % 2 == 0


# Test our is_even function using map.
# Map is a high-order function that receives a function and a list
# It returns a new list with the function applied to each element
for e in map(is_even, [1, 2, 3]):
    print(e)

# Test our is_even function using filter.
# Filter is a high-order function that receives a function and a list
# It returns a new list with only those elements that evaluate the function to True
for e in map(is_even, [1, 2, 3]):
    print(e)


def exists_any(criteria, tuple_of_3_elements):
    """
    Our own high-order function that checks if any elements in a tuple satisfies a criteria
    :param criteria: a function that represents a criteria
    :param tuple_of_3_elements:  a tuple to evaluate some criteria
    :return: a Boolean indicating if at least one element satisfies the criteria function
    """
    return criteria(tuple_of_3_elements[0]) or \
           criteria(tuple_of_3_elements[1]) or \
           criteria(tuple_of_3_elements[2])

# Test our exists_any function with is_even
print(exists_any(is_even, (1, 2, 5)))
